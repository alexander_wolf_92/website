import { Injectable } from '@angular/core';

// router
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

// rxjs
import { Observable } from 'rxjs';

// services
import { STORAGE_KEY_AUTH_TOKEN } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  constructor(private _router: Router) {}

  canActivate (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN)) {
      return true;
    } else { 
      return new Promise((resolve) => {
        (this._router.navigate(['/login']));

        return resolve(false);
      });
    }
  }
}
  

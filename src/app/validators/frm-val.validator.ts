import { AbstractControl, ValidatorFn } from "@angular/forms";

export const prohibitedValidator = (...values: string[]): ValidatorFn => {
    return (c: AbstractControl) => {
        for (const value of values) {
            if (value === c.value) {
                return {
                    forbidden: true
                };
            }
        }
        
        return null;
    }
}
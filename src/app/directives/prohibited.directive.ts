import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { prohibitedValidator } from '../validators/frm-val.validator';

@Directive({
  providers: [{
    multi: true,
    provide: NG_VALIDATORS,
    useExisting: ProhibitedDirective
}],
  selector: '[appProhibited]'
})
export class ProhibitedDirective {
  
  @Input('appProhibited') public invalidWords!: string;
  
  constructor() { }

  public validate(control: AbstractControl): ValidationErrors | null {
    return prohibitedValidator(this.invalidWords)(control);
  }
}

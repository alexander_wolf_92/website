// Modules:
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { SwiperModule } from 'swiper/angular';

// Directives
import { ProhibitedDirective } from './directives/prohibited.directive';

// Components
import { AppComponent } from './app.component';
import { FavouritesComponent } from './components/favourites/favourites.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { LoginFooterComponent } from './components/login-footer/login-footer.component';
import { MovieComponent } from './components/movie/movie.component';
import { MovieListComponent } from './components/movie-list/movie-list.component';
import { MoviesDetailComponent } from './components/movies-detail/movies-detail.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { SearchedTitlesComponent } from './components/searched-titles/searched-titles.component';
import { SignupComponent } from './components/signup/signup.component';

@NgModule({
  declarations: [
    AppComponent,
    FavouritesComponent,
    FooterComponent,
    HomepageComponent,
    LoginComponent,
    LoginFooterComponent,
    MovieComponent,
    MovieListComponent,
    MoviesDetailComponent,
    NavbarComponent,
    ProhibitedDirective,
    ProhibitedDirective,
    SearchbarComponent,
    SearchedTitlesComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatSliderModule,
    NgbModule,
    SwiperModule,
  ],
  providers: [
   
],
  bootstrap: [AppComponent]
})
export class AppModule { }

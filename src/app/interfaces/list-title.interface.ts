export interface IListTitle {
    id: string;
    rank: string;
    title: string;
    fullTitle: string;
    year: string;
    image: string;
    crew: string;
    imdbRating: string;
    imdbRatingCount: string;
    favorite: boolean
}
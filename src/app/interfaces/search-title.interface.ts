export interface ISearchTitle {
    id: string;
    resultType: string;
    image: string;
    title: string;
    description: string;
}
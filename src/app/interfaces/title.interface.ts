import { TrailerData } from "../models/trailer-data.model";
import { ISimilarShort } from "./similar-short.interface";

export interface ITitle {
    image: string;
    id: string;
    title: string;
    favorite: boolean;
    year: string;
    runtimeMins: string;
    plot: string;
    type: string;
    genres: string;
    trailer: TrailerData;
    similars: ISimilarShort[];
}
import { IListTitle } from "./list-title.interface";

export interface IKeyword {
    keyword: string;
    items: IListTitle[];
    errorMessage: string;
}
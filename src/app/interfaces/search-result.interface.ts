import { ISearchTitle } from "./search-title.interface";

export interface IResult {
    searchType: string;
    expression: string;
    results: ISearchTitle[];
}
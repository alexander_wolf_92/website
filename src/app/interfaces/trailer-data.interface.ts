export interface ITrailerData {
    imdbId: string;
    title: string;
    fullTitle: string;
    type: string;
    year: string;
    videoId: string;
    videoTitle: string;
    videoDescription: string;
    thumbnailUrl: string;
    link: string;
    linkEmbed: string;
    errorMessage: string;
}
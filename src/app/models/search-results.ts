import { SearchTitle } from "./search-title";

export class SearchResults{
    constructor(
        public results: SearchTitle[]
    ) {
        //
    }
}
import { SimilarShort } from "./similar-short.model";
import { TrailerData } from "./trailer-data.model";

export class Title {
    constructor(
        public image: string,
        public id: string,
        public title: string,
        public favorite: boolean,
        public year?: string,
        public runtimeMins?: string,
        public plot?: string,
        public type?: string,
        public genres?: string,
        public trailer?: TrailerData,
        public similars?: SimilarShort[]
    ) {
        // 
    }
}
export class ListTitle {
    constructor (
        // id is a string because it follows the API template 'tt<number>'
        public readonly id: string,
        public image: string,
        public favorite: boolean,
        public title?: string,
        public rank?: string,
        public fullTitle?: string,
        public year?: string,
        public crew?: string,
        public imdbRating?: string,
        public imdbRatingCount?: string
    ) {
        //
    }
}
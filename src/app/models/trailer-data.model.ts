export class TrailerData {
    constructor (
        public imdbId?: string,
        public title?: string,
        public year?: string,
        public videoId?: string,
        public videoTitle?: string,
        public videoDescription?: string,
        public thumbnailUrl?: string,
        public link?: string,
        public linkEmbed?: string,
        public errorMessage?: string,
    ) {

    }
}
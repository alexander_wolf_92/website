import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { FavouritesComponent } from './components/favourites/favourites.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { MovieComponent } from './components/movie/movie.component';
import { MovieListComponent } from './components/movie-list/movie-list.component';
import { MoviesDetailComponent } from './components/movies-detail/movies-detail.component';
import { SearchedTitlesComponent } from './components/searched-titles/searched-titles.component';
import { SignupComponent } from './components/signup/signup.component';

// Guards
import { IsLoggedGuard } from './guards/is-logged.guard';

const routes: Routes = [{
  canActivate: [IsLoggedGuard],
  component: FavouritesComponent,
  path: 'favorites'
}, {
  // canActivate: [IsLoggedGuard],
  component: HomepageComponent,
  path: ''
}, {
  component: LoginComponent,
  path: 'login'
}, {
  canActivate: [IsLoggedGuard],
  component: MovieComponent,
  path: 'movie/:id'
}, {
  canActivate: [IsLoggedGuard],
  component: MovieListComponent,
  path:'movie-list'
}, {
  canActivate: [IsLoggedGuard],
  component: MoviesDetailComponent,
  path: 'movie-detail/:id'
}, {
  canActivate: [IsLoggedGuard],
  component: SearchedTitlesComponent,
  path: 'searched-titles'
}, {
  component: SignupComponent,
  path: 'signup'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

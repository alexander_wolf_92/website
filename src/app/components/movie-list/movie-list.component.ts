import { Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';

import { Subscription } from 'rxjs';

// models
import { ListTitle } from 'src/app/models/list-title';
import { Title } from 'src/app/models/title';

// services
import { MovieDetailService } from 'src/app/services/movie-detail.service';

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";


// install Swiper modules
SwiperCore.use([Navigation]);

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MovieListComponent implements OnDestroy, OnInit {

  private _titles$!: Subscription;

  @Input() public genre!: string;
  public titles!: ListTitle[];

  constructor(private _movieService: MovieDetailService) { }

  ngOnDestroy(): void {
    this._titles$.unsubscribe();
  }

  ngOnInit(): void {
    this._titles$ = this._movieService.getKeywordTitle(this.genre).subscribe(titles => {
      this.titles = titles;

      return this.titles = titles.splice(0, 9);
    });
  }
}

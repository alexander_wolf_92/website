import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  
  public hidePass: string = "../../../assets/svg/eye_unlock.svg";
  public email!: string;
  public eye: string = "../../../assets/svg/eye.svg";
  public passType: string = 'password';
  public password!: string;
  public showPass: string = "../../../assets/svg/eye.svg";

  constructor() { }

  public handlePassword() {
    if (this.passType === 'password')  {
       this.passType = 'text';
       this.eye = this.hidePass;
    } else {
      this.passType = 'password';
      this.eye = this.showPass;
    }
  }

  ngOnInit(): void {
  }

}
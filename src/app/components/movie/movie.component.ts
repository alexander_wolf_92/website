import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subscription, switchMap } from 'rxjs';

// Models
import { TrailerData } from 'src/app/models/trailer-data.model';

// Services
import { MovieDetailService } from 'src/app/services/movie-detail.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnDestroy, OnInit {

  private _movieTrailer$!: Subscription;

  public trailer!: TrailerData;

  constructor(private _route: ActivatedRoute, private _getTrailer: MovieDetailService) { }

  ngOnDestroy(): void {
    this._movieTrailer$.unsubscribe();
  }

  ngOnInit(): void {
    this._movieTrailer$ = this._route.paramMap.pipe(
      map(p => p.get('id') as string),
      switchMap(id => this._getTrailer.getTrailer(id))
    ).subscribe(object => this.trailer = object);
  }

}

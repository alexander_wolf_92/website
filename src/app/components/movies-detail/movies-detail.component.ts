import { Component, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subscription, switchMap } from 'rxjs';

// Models
import { Title } from 'src/app/models/title';

// Services
import { MovieDetailService } from 'src/app/services/movie-detail.service';

@Component({
  selector: 'app-movies-detail',
  templateUrl: './movies-detail.component.html',
  styleUrls: ['./movies-detail.component.scss']
})
export class MoviesDetailComponent implements OnDestroy, OnInit {

  private _movieDetail$!: Subscription;

  public favTitles: Title[] = [];
  public title!: Title;

  constructor(private _route: ActivatedRoute, private _getPreview: MovieDetailService) { }

  public setFavTitle(fave: boolean, id: string) {
    if (!!fave === false) {
      this._getPreview.getPreview(id).subscribe(favTitles => {
        this._getPreview.addFavTitle(favTitles);

        return !fave;
      });
    }
  }

  ngOnDestroy(): void {
    this._movieDetail$.unsubscribe();
  }

  ngOnInit(): void {
    this._movieDetail$ = this._route.paramMap.pipe(
      map(p => p.get('id') as string),
      switchMap(id => this._getPreview.getPreview(id))
    ).subscribe(object => this.title = object);
  }

}

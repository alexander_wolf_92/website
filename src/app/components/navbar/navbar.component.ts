import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

// Services
import { MovieDetailService } from 'src/app/services/movie-detail.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  public a: string = 'Show';
  public search!: string;
  public show: boolean = false;
  public showD: boolean = false;
  public ul: string = 'ShowD';

  constructor(private _searchService: MovieDetailService) { }

  public dropdown(): void {
    this.showD = !this.showD;

    if (this.show) {
      this.ul = "Hide";
    } else {
      this.ul = "ShowD";
    }
  }

  public handleLogOut(): void {
    window.sessionStorage.clear();
  }

  public handleSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }   

    this._searchService.searchTitle(this.search).subscribe(movies => {
      this._searchService.setTitles(movies);
    });
  }

  public toggle(): void {
    this.show = !this.show;

    if (this.show) {
      this.a = "Hide";
    } else {
      this.a = "Show";
    }
  }

  ngOnInit(): void {
  }

}

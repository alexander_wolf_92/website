import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

// Services
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  public email!: string;
  public eye: string = "../../../assets/svg/eye.svg";
  public hidePass: string = "../../../assets/svg/eye_unlock.svg";
  public password!: string;
  public passType: string = 'password';
  public showPass: string = "../../../assets/svg/eye.svg";

  constructor(private _userService: UserService) { }

  public handleSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }   
    
    this._userService.login(this.email, this.password).subscribe(res => console.log(res));
  }

  public handlePassword() {
      if (this.passType === 'password')  {
        this.passType = 'text';
        this.eye = this.hidePass;
      } else {
        this.passType = 'password';
        this.eye = this.showPass;
      }
  }

  ngOnInit(): void {
  }

}

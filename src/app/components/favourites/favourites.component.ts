import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

// Models
import { Title } from 'src/app/models/title';

// Services
import { MovieDetailService } from 'src/app/services/movie-detail.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {

  public favTitle!: string;
  public favTitles$: Observable<Title[]> = this._searchService.favTitles$;

  constructor(private _searchService: MovieDetailService) { }

  ngOnInit(): void {
  }

}

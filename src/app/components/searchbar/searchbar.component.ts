import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms';

// Services
import { MovieDetailService } from 'src/app/services/movie-detail.service';


@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {
  
  public search!: string;

  constructor(private _searchService: MovieDetailService) { }

  public handleSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }

    this._searchService.searchTitle(this.search).subscribe(movies => {
      this._searchService.setTitles(movies);
    });
  }

  ngOnInit(): void {
  }
}

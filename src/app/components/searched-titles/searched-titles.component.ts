import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

// Models
import { SearchTitle } from 'src/app/models/search-title';

// Services
import { MovieDetailService } from 'src/app/services/movie-detail.service';

@Component({
  selector: 'app-searched-titles',
  templateUrl: './searched-titles.component.html',
  styleUrls: ['./searched-titles.component.scss']
})
export class SearchedTitlesComponent implements OnInit {

  public movies$: Observable<SearchTitle[]> = this._searchTitle.movies$;

  constructor(private _searchTitle: MovieDetailService) { }

  ngOnInit(): void {
  }

}

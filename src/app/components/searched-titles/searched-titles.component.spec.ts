import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchedTitlesComponent } from './searched-titles.component';

describe('SearchedTitlesComponent', () => {
  let component: SearchedTitlesComponent;
  let fixture: ComponentFixture<SearchedTitlesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchedTitlesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchedTitlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

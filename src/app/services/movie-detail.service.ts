import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, map, Observable } from 'rxjs';

// Interfaces
import { IKeyword } from '../interfaces/keyword.interface';
import { IListTitle } from '../interfaces/list-title.interface';
import { IResult } from '../interfaces/search-result.interface';
import { ISearchTitle } from '../interfaces/search-title.interface';
import { ITitle } from '../interfaces/title.interface';
import { ITrailerData } from '../interfaces/trailer-data.interface';

// Models
import { ListTitle } from '../models/list-title';
import { SearchTitle } from '../models/search-title';
import { Title } from '../models/title';
import { TrailerData } from '../models/trailer-data.model';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailService {

  // To search titles:
  private _movies: SearchTitle[] = [];
  private _movies$ = new BehaviorSubject<SearchTitle[]>(this._movies);
  public movies$ = this._movies$.asObservable();


  // To manage favorites:
  private _favTitles: Title[] = [];
  private _favTitles$ = new BehaviorSubject<Title[]>(this._favTitles);

  public favTitles$ = this._favTitles$.asObservable();

  constructor(private _http: HttpClient) { }


  // To search titles:
  private _objSearchTitle (obj: ISearchTitle): SearchTitle {
    return new SearchTitle(
      obj.id,
      obj.image,
      obj.title
    );
  }

  public addTitle(m : SearchTitle): void {
    this._movies.push(m);
    this._movies$.next(this._movies);
  }

  public addTitles(movies: SearchTitle[]): void {
    this._movies.push(...movies);
    this._movies$.next(this._movies);
  }

  public setTitles(movies: SearchTitle[]): void {
    this._movies = movies;
    this._movies$.next(this._movies);
  }

  public searchTitle(value:string): Observable<SearchTitle[]> {
    return this._http.get<IResult>(`${environment.api.url}/Search/${environment.key}/${value}`).pipe(
      map(result => result.results.map(title => this._objSearchTitle(title)))
    );
  } 


  // To get genre lists:
  private _objToKeywordModel(obj: IListTitle): ListTitle {
    return new Title(obj.image, obj.id, obj.title, obj.favorite);
  }

  public getKeywordTitle(genre: string): Observable<ListTitle[]> {
    return this._http.get<IKeyword>(`${environment.api.url}/Keyword/${environment.key}/${genre}`).pipe(
      map(titles => titles.items.map(title => this._objToKeywordModel(title)))
    );
  }


  // To manage favorites:
  public addFavTitle(title: Title): void {
    this._favTitles.push(title);
    this._favTitles$.next(this._favTitles);
  }

  public addFavTitles(titles: Title[]): void {
    this._favTitles.push(...titles);
    this._favTitles$.next(this._favTitles);
  }

  public setFavTitles(titles: Title[]): void {
    this._favTitles = titles;
    this._favTitles$.next(this._favTitles);
  }


  // To get details:
  private _objToTitle(obj: ITitle): Title {
    return new Title(
      obj.image,
      obj.id,
      obj.title,
      obj.favorite,
      obj.year,
      obj.runtimeMins,
      obj.plot,
      obj.type,
      obj.genres,
      obj.trailer,
      obj.similars
    );
  }

  public getPreview(id: string): Observable<Title> {
    return this._http.get<ITitle>(`${environment.api.url}/Title/${environment.key}/${id}`).pipe(
      map(title => this._objToTitle(title))
    );
  }


  // To get trailers:
  private _objToTrailer(obj: ITrailerData): TrailerData {
    return new TrailerData(
      obj.imdbId,
      obj.title,
      obj.year,
      obj.videoId,
      obj.videoTitle,
      obj.videoDescription,
      obj.thumbnailUrl,
      obj.link,
      obj.linkEmbed,
      obj.errorMessage
    );
  }

  public getTrailer(imdbId: string): Observable<TrailerData> {
    return this._http.get<ITrailerData>(`${environment.api.url}/Trailer/${environment.key}/${imdbId}`).pipe(
      map(title => this._objToTrailer(title))
    );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

// Interfaces
import { ILogin } from '../interfaces/login.interface';

export const STORAGE_KEY_AUTH_TOKEN = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) { }
  
  public getToken(): string {
    return sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN) as string;
  }

  public hasToken(): boolean {
     return !!sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN);
  }

  public login(email: string, password: string) {
    return this._http.post<ILogin>('https://reqres.in/api/login', {
      email, 
      password
    }, {
      observe: 'response'
    }).pipe(
    map(response => {
      if (response.ok) {
        sessionStorage.setItem(STORAGE_KEY_AUTH_TOKEN, response.body?.token as string);
        console.log(response.body?.token);
        
        return true;
      }

      return false;
    })
    );
  }
}
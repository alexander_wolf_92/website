import { TestBed } from '@angular/core/testing';

import { SignupTokenInterceptor } from './signup-token.interceptor';

describe('SignupTokenInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      SignupTokenInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: SignupTokenInterceptor = TestBed.inject(SignupTokenInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { LoginTokenInterceptor } from './login-token.interceptor';

describe('LoginTokenInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      LoginTokenInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: LoginTokenInterceptor = TestBed.inject(LoginTokenInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

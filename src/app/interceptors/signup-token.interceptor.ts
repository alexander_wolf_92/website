import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// Services
import { UserService } from '../services/user.service';

@Injectable()
export class LoginTokenInterceptor implements HttpInterceptor {

  constructor(private _userService: UserService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const req = request.clone();
    
    if (this._userService.hasToken()) {
      req.headers.append('Authorization', `Bearer ${this._userService.getToken()}`);
    }

    return next.handle(req);
  }
}

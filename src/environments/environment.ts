// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  api: {
    url: 'https://imdb-api.com/en/API'
  },
  key: 'k_ow62f5bf',
  production: false
};

// Alberto: k_83g445xc
// Francesca: k_0dxco9aw
// Francesca2: k_xlltyxr1
// Francesca3: k_3269uryz
// Francesca4: k_9er50jwh
// Johanna: k_j1o5tt64
// Ale C: k_mimvkbkm
// Ale C2: k_q6cphmb9
// Ale C3: k_8ya1hr68
// Ale DT: k_oec7j5od
// Fra: k_ow62f5bf
// Fra: k_lh41x3sv
// k_8ya1hr68

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
